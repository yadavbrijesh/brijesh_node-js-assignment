const fs = require('fs');

const requestHandler = (req, res) => {
    const url = req.url;
    const method = req.method;

    if (url === '/') {
        res.write('<html>');
        res.write('<head><title>Enter Message</title></head>');
        res.write('<body><h1>Hello Node js App </h1><form action= "/message" method= "POST"><input type= "text" name= "email" placeholder= "Enter Your Email" ><input type= "text" name= "message" placeholder= "Enter Your Message" ><button type="submit">Send</button></form></body>');
        res.write('</html>');
        return res.end();
        }
        if(url === '/message' &&  method=== 'POST') {
            const body = [];
            req.on('data', (chunk) => {
                console.log(chunk);
                body.push(chunk);
            });
            return req.on('end', () =>{
                const parsedBody = Buffer.concat(body).toString();
                console.log(parsedBody);
                const message = (parsedBody.split('+').join(' '));
                console.log(message);
                fs.writeFile('message.txt', message,  (err) => {
                    res.statusCode = 302;
                    res.setHeader( 'Location', '/');
                    return res.write('Your response has beeen submitted' );                
                });
        
               
            });       
        }
        res.setHeader('Content-type', 'text/html');
        res.write('<html>');
        res.write('<head><title>First Page</title></head>');
        res.write('<body> <h1>Hello From my Node.js server!</h1></body>');
        res.write('</html>');
        res.end();   

}

module.exports = requestHandler;
